libconfig-apacheformat-perl (1.2-6) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 11 Jun 2022 23:03:38 +0100

libconfig-apacheformat-perl (1.2-5.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Wed, 06 Jan 2021 19:29:33 +0100

libconfig-apacheformat-perl (1.2-5) unstable; urgency=medium

  * Team upload

  [ gregor herrmann ]
  * Add debian/README.source to document quilt usage, as required by
    Debian Policy since 3.8.0.
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ gregor herrmann ]
  * Change my email address.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Axel Beckert ]
  * Fix Homepage header (module vs distribution name, thanks DUCK!)

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Remove Stephen Gran from Uploaders on request of the MIA team.
    (Closes: #838400)

  [ Florian Schlichting ]
  * Add headers to apache_globs.patch
  * Switch to source format 3.0 (quilt)
  * Bump dh compat to level 9
  * Brush up short and long description
  * Use short-form debian/rules
  * Convert d/copyright to copyright-format 1.0
  * Declare compliance with Debian Policy 3.9.8
  * Mark package autopkgtest-able
  * Add a patch to fix POD and spelling errors

 -- Florian Schlichting <fsfs@debian.org>  Tue, 22 Nov 2016 22:05:13 +0100

libconfig-apacheformat-perl (1.2-4) unstable; urgency=low

  [ Stephen Gran ]
  * Add watch file
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza).
  * quiltify file glob patch

  [ gregor herrmann ]
  * Add libclass-methodmaker-perl to build dependencies to fix a FTBFS bug
    (closes: #470264).
  * debian/copyright: fix download URL.
  * Add /me to Uploaders.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Mon, 10 Mar 2008 19:03:59 +0100

libconfig-apacheformat-perl (1.2-3) unstable; urgency=low

  * Add to Debian Perl group

 -- Stephen Gran <sgran@debian.org>  Sun, 02 Mar 2008 02:00:33 +0000

libconfig-apacheformat-perl (1.2-2) unstable; urgency=low

  * Update to work with apache file globs such as
    Include /etc/apache2/mods-enabled/*.load

 -- Stephen Gran <sgran@debian.org>  Sat, 01 Mar 2008 15:53:59 +0000

libconfig-apacheformat-perl (1.2-1) unstable; urgency=low

  * Initial Release.

 -- Stephen Gran <sgran@debian.org>  Sat, 01 Mar 2008 14:57:43 +0000
